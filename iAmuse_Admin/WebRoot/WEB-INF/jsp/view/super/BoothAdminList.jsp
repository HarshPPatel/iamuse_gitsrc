<%@ include file="/WEB-INF/jsp/include/taglibs.jsp"%>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="cg" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>	
<script >
$(document).ready(function() {
$("#c").addClass("active_menu");
});
</script>
<div class="right-pannel">
					<c:if test="${not empty successMessage}">
					<div id="effect"  class=""><center><h4 style="color: green;">${successMessage}</h4></center></div>
					</c:if>
					<c:if test="${not empty errorMessage}">
					<div id="effect"  class=""><center><h4 style="color: red;">${errorMessage}</h4></center></div>
					</c:if>
					<h1 class="heading pull-left" style="padding-top:15px;">Booth Admin</h1>
					<div class="clearfix"></div>
					<div class="inner-content">
						<div class="col-row">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Location</th>
										<th>Subscription</th>
										<th>Phone No</th>
										<th>E-mail Id</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${boothAdminLoginList}" var="item">
								<tr>
								<td>${item.username}</td>
								<td>${item.location}</td>
								<td>
								<c:forEach items="${subList}" var="sl">
								<c:if test="${item.subId == sl.subId }">
								${sl.subName}
								</c:if>
								</c:forEach>
								</td>
								<td>${item.contactNumber}</td>
								<td>${item.emailId}</td>
								 <td> 
								<c:if test="${item.subId == 1 || item.createdDate > item.subEndDateFormat}">
								 <%-- <c:if test="${currentDate > item.subUpdatedDateFormat}"> --%>
								 <a href="upgradeSubscriptionMail?emailId=${item.emailId}"><button class="btn btn-green">Sent</button></a> 
								 <%-- </c:if> --%>
								 </c:if>
								 </td>
								</tr>
								</c:forEach> 
								</tbody>
							</table>
							
							<div class="pageing">
								<c:set var="pC" value="${pageCount}"/>
      								<c:set var="pId" value="${pageid}"/>
								<ul class="pagination">
									<li ><c:if test="${pId > 1}"><a href="getBoothAdminList?pageid=${pageid-1}&total=${total}">&laquo;</a></c:if> </li>
									<c:forEach var="p" begin="1" end="${pageCount}"><li><a href="getBoothAdminList?pageid=${p}&total=${total}">${p}</a></li></c:forEach>
									<li><c:if test="${pId < pC}"><a href="getBoothAdminList?pageid=${pageid+1}&total=${total}">&raquo;</a></c:if> </li> 
								</ul>
							</div>
							
						</div>
					</div>
				</div>

				<div class="clearfix"></div>