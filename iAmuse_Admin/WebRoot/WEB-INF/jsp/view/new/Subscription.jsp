	<%@include file="/WEB-INF/jsp/include/taglibs.jsp"%>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/dataTable.css" />
<script src="<%=request.getContextPath()%>/resources/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable({
	    	 "language": {
	             "lengthMenu": "Show _MENU_ ",
	             "zeroRecords": "No previous payments",
	             "info": "Showing page _PAGE_ of _PAGES_",
	             "infoEmpty": "No records available",
	             "infoFiltered": "(filtered from _MAX_ total records)"
	         }
	    });
	} );
</script>	
<script type="text/javascript">
      $(document).ready(function() {
    	  var subId=document.getElementById("subid").value;
    	
    	  $( "#orderButton2" ).click(function() {
    	       if(subId>2)
    	        {
    	        alert("you have already upgraded plan");
    	       return false;
    	        }
    	             
    	      });
    	      $( "#orderButton3" ).click(function() {
    	       if(subId>3)
    	    {
    	        alert("you have already upgraded plan");
    	   return false;
    	    }
    	      });
    	  
    	  
            $("#d").addClass("active_menu");
            
            $('#effect').delay(10000).fadeOut(400);
            if(subId==1){
            	$("#abc1").addClass("active-rate-box");
            }else if(subId==2){
            	$("#abc2").addClass("active-rate-box");
            	$("#aa2").hide();
            }else if(subId==3){
            	$("#abc3").addClass("active-rate-box");
            	$("#aa3").hide();
            }else{
            	$("#abc4").addClass("active-rate-box");
            	$("#aa4").hide();
            }
        });
      function upgrade(){
    	   window.location.href ="<%=request.getContextPath()%>/upgradePlan";
      }
      </script>
      <style>
      	.right-pannel{height:auto !important;}
      </style>
     	<script>
var end =  new Date('${boothAdminLogin1.subEndDate}');
if(end=='Invalid Date'){
}else{

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {

            clearInterval(timer);
            document.getElementById('countdown').innerHTML = ' EXPIRED!';

            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        document.getElementById('countdown').innerHTML = 'Your Subscription Will Expire After :  ';
        document.getElementById('countdown').innerHTML += days + ' Days  ';
        document.getElementById('countdown').innerHTML += hours + ' Hrs  ';
        document.getElementById('countdown').innerHTML += minutes + ' Mins  ';
        document.getElementById('countdown').innerHTML += seconds + ' Secs ';
    }
	}
    timer = setInterval(showRemaining, 1000);
</script>
	
      <input type="hidden" value="${boothAdminLogin1.subId}" id="subid"/>
<div class="right-pannel" >
					<c:if test="${not empty successMessage}">
					<div id="effect"  class=""><center><h4 style="color: green;">${successMessage}</h4></center></div>
					</c:if>
					
					<c:if test="${not empty errorMessage}">
					<div id="effect"  class=""><center><h4 style="color: red;">${errorMessage}</h4></center></div>
					</c:if>
					<h1 class="heading pull-left">Choose The Plan That Works Best</h1>
					<div class="clearfix"></div>
					<div class="inner-content" style="padding:15px 20px 20px;">
						<div class="col-row">
							<b><div id="countdown"></div></b>
							<div class="login_form" style="width:100%;margin:0px;">
								<!-- <h1 style="color:#717171;font-size:18px;margin-top:25px;">To upgrade, choose subscription plan based on your preferences</h1> -->
								<c:forEach items="${subscriptionMaster}" var="sm" varStatus="loop">
								<div class="plan-rate-box" style="margin:0px auto;">
								
									<div class="rate-box" id="abc${loop.index + 1 }" style="">
									<c:if test="${sm.subPrice != 0 && sm.subPrice != 100}">
										<div class="g-pricingtable-ribbon">
											<img src="<%=request.getContextPath()%>/resources/img/single_event.png">
										</div> 
									</c:if>
									<c:if test="${sm.subPrice == 100}">
										<div class="g-pricingtable-ribbon">
											<img src="<%=request.getContextPath()%>/resources/img/unlimited.png">
										</div> 
									</c:if>
										<div style="/* height:auto; */padding:15px;">
											<h1>${sm.subName}</h1>
											<c:if test="${sm.subPrice==0}">
											<p class="dollar">FREE</p>
											</c:if>
											<c:if test="${sm.subPrice!=0}">
											<p class="dollar">$&nbsp;${sm.subPrice}</p>
											</c:if>
											<p class="dollar-text"><c:if test="${sm.subPrice==0}" >limited features</c:if><c:if test="${sm.subPrice==10}" >Expires after 24 Hours</c:if><c:if test="${sm.subPrice==25}" >Expires after 24 Hours</c:if><c:if test="${sm.subValidaityDayPeriod!=1}">monthly</c:if></p>
											<p class="dollar-para">${sm.description}</p>
											<!-- <p class="dollar-para">Backgrounds for 24 hrs </p> -->
											<div class="border-line"></div>
											<c:set var="string2" value="${sm.deviceDescription}" />
											    <c:forEach items="${string2}" var="emps1">
											            <p class="device-desc"><c:out value="${emps1}"/></p>
											    </c:forEach>
											<div class="clearfix"></div>
										</div>
										<c:if test="${sm.subPrice != 0}">
										<a href="detailSubscriptionPlan?id=${sm.subId}" id="aa${loop.index+1}"><button  class="btn btn-green" id="orderButton${loop.index+1}">Order Now</button></a>
										</c:if>
										<c:if test="${sm.subId == boothAdminLogin1.subId}">
										<a href="javascript:void(0)" ><button class="btn btn-green" style="border-radius: 0px;padding: 13px;">Your Current Plan</button></a>
										</c:if>
										<div class="clearfix"></div>
									</div>
									
								</div>
								 </c:forEach>
								 
								 <c:if test="${boothAdminLogin1.loginTour==0 && boothAdminLogin1.subId!=1 || boothAdminLogin1.subId==1}">
									<center><a href="getRegisteredDeviceConfig"><button class="btn btn-green" style="margin:20px auto;">Start configuring your Booth & Event</button></a></center>
									<div class="clearfix"></div>
								</c:if>
								 <div class="clearfix"></div>
							</div>
						</div>
					</div>
	<div class="clearfix"></div>
	<%-- <c:if test="${transactionHistoryVOs.size}"> --%>
	<h2 class="heading" style="margin:25px 0px -20px">Payment History</h2>

<div style="background:#fff;padding:20px;border-radius:5px;border:1px solid #e2e2e2;margin:30px 0px">	
	<table id="example" class="display" cellspacing="0" width="100%">
	<thead>
  <tr>
    <th>Transaction Id</th>
    <th>Payment Type</th>
    <th>Payment Date</th>
    <th>Payment Amount</th>
    <th>Subscription Name</th>
     <th>Status</th>
  </tr>
  </thead>
  <tbody>
  <c:forEach items="${transactionHistoryVOs }" varStatus="loop" var="th">
  <tr>
    <td>${th.txnId}</td>
    <td>${th.paymentType}</td>
    <td>${th.paymentDate}</td>
    <td>$&nbsp;${th.paymentAmount}</td>
    <td>${th.itemName}</td>
    <td>${th.statusResponse}</td>
  </tr>
  </c:forEach>
  </tbody>
</table>
</div>
<%-- </c:if>	 --%>
<div class="clearfix"></div>
</div>
	