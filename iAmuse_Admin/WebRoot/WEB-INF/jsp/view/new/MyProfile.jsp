<%@include file="/WEB-INF/jsp/include/taglibs.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	 $('#effect').delay(9000).fadeOut(400);
})
</script>
		<div class="right-pannel">
					<c:if test="${not empty successMessage}">
					<div id="effect"  class=""><center><h4 style="color: green;">${successMessage}</h4></center></div>
					</c:if>
					
					<c:if test="${not empty errorMessage}">
					<div id="effect"  class=""><center><h4 style="color: red;">${errorMessage}</h4></center></div>
					</c:if>
					<h1 class="heading pull-left" style="padding-top:15px;">My Profile</h1>
					<div class="clearfix"></div>
					<div class="inner-content profile-content">
						<div class="col-row">
							<c:if test="${not empty boothAdminLogin2.imageFileName}">
							<div class="profile-img">
							<img src="<%=request.getContextPath()%>/imageDisplay?id=${boothAdminLogin2.userId}">
							<%-- <img src="<%=request.getContextPath()%>/resources/images/images/admin_change.png"> --%>
							</div>
							</c:if>
							
							<c:if test="${empty boothAdminLogin2.imageFileName}">
							<div class="profile-img">
							<img src="<%=request.getContextPath()%>/resources/images/images/admin_change.png">
							</div>
							</c:if>
							<div class="profile-detail">
								<div class="event-row">
									<div class="event-label">Name</div>
									<div class="label-value">${boothAdminLogin2.username}</div>
									<div class="clearfix"></div>
								</div>
								<div class="event-row">
									<div class="event-label">Email Id</div>
									<div class="label-value">${boothAdminLogin2.emailId}</div>
									<div class="clearfix"></div>
								</div>
								<div class="event-row">
									<div class="event-label">Phone Number</div>
									<div class="label-value">${boothAdminLogin2.contactNumber}</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="profile-config">
								<div class="edit-btn"><a href="editProfileDetails" class="btn btn-green">Edit</a></div>
								<div class="delete-btn"><span onclick="deleteUserProfile();" class="btn btn-green zscale">Delete</span></div>
							</div>
							<div class="clearfix"></div><br>
							<b><div id="profileCountdown"></div></b>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
<script>
	var end = new Date('${boothAdminLogin2.subEndDate}');
	if (end == 'Invalid Date') {
	} else {
		var profile = {}
		profile.second = 1000;
		profile.minute = profile.second * 60;
		profile.hour = profile.minute * 60;
		profile.day = profile.hour * 24;
		var timer;

		function showRemaining() {
			var now = new Date();
			var distance = end - now;
			if (distance < 0) {
				clearInterval(timer);
				document.getElementById('profileCountdown').innerHTML = ' EXPIRED!';
				return;
			}
			var days = Math.floor(distance / profile.day);
			var hours = Math.floor((distance % profile.day) / profile.hour);
			var minutes = Math.floor((distance % profile.hour) / profile.minute);
			var seconds = Math.floor((distance % profile.minute) / profile.second);

			document.getElementById('profileCountdown').innerHTML = 'Your Subscription Will Expire After :  ';
			document.getElementById('profileCountdown').innerHTML += days + ' Days  ';
			document.getElementById('profileCountdown').innerHTML += hours + ' Hrs  ';
			document.getElementById('profileCountdown').innerHTML += minutes + ' Mins  ';
			document.getElementById('profileCountdown').innerHTML += seconds + ' Secs ';
		}
	}
	timer = setInterval(showRemaining, 1000);
	function deleteUserProfile(){
		var r = confirm("Are you sure you want to delete this profile?");
		if (r == true) {
		 	$.ajax({url: "<%=request.getContextPath()%>/deleteProfileDetail", 
				success: function(result){
			    	alert("Successfully deleted");
			  }}
			);
		}
	}
</script>