<script>
function myFunction() {
	$('.logout-section').css('display','none');
}

</script>

<%@include file="/WEB-INF/jsp/include/taglibs.jsp"%>


		<div class="header">
				<div class="container-1">
					<div class="logo">
						<img src="<%=request.getContextPath()%>/resources/images/images/logo.png">
					</div>
					<div class="profile_tab">
							<c:if test="${not empty boothAdminLogin2.image}">
							<div class="profile-pic">
							<img  src="<%=request.getContextPath()%>/imageDisplay?id=${boothAdminLogin2.userId}">
							<%-- <img src="<%=request.getContextPath()%>/resources/images/images/admin_change.png"> --%>
							</div>
							</c:if>
							
							<c:if test="${empty boothAdminLogin2.image}">
							<div class="profile-pic">
								<img src="<%=request.getContextPath()%>/resources/images/images/admin_change.png">
							</div>
							</c:if>
						<div class="profile-name">
							<p>hello,&nbsp; ${boothAdminLogin2.username}<p>
							<p>Welcome Back<p>
							<img src="<%=request.getContextPath()%>/resources/images/images/drop-down.png" class="drop-down">
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
						<div class="logout-section" onclick="myFunction()">
							<div class="logout-box">
								<ul>
									<li><a href="getProfileDetails">View Profile</a></li>
									<li style="border-bottom:0px"><a href="SignOut">Logout</a></li>
								</ul>
							</div>
						</div>
						<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
<script>
$(document).ready(function(){

		$(window).resize(function() {
			var header_height=$('.header').height();
			//alert(header_height);
			$('.right-pannel').height($(window).height() - (header_height+50));
			$('.logout-section').height($(window).height() - (header_height));
		});
		$(window).trigger('resize');
		$('.drop-down').click(function(){
			$('.logout-section').toggle();			
		});
		$('.logout-box ul li a').click(function(){
			$('.logout-section').hide();
		});
})
</script>