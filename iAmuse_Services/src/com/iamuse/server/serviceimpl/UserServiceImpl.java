package com.iamuse.server.serviceimpl;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SessionFactory;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.amuse.server.dao.UserDao;
import com.google.gson.Gson;
import com.iamuse.server.entity.Adminboothevent;
import com.iamuse.server.entity.BoothAdminLogin;
import com.iamuse.server.entity.DeviceIp;
import com.iamuse.server.entity.DeviceRegistration;
import com.iamuse.server.entity.Fovbyuser;
import com.iamuse.server.entity.SubscriptionMaster;
import com.iamuse.server.entity.TransactionMaster;
import com.iamuse.server.entity.UploadImage;
import com.iamuse.server.requestVO.BaseRequestVO;
import com.iamuse.server.requestVO.BoothAdminRegistrationRequestVO;
import com.iamuse.server.requestVO.CrashLogsRequestVO;
import com.iamuse.server.requestVO.DeviceIPRequestVO;
import com.iamuse.server.requestVO.DeviceRegistrationRequestVO;
import com.iamuse.server.requestVO.DeviceTokenRequestVO;
import com.iamuse.server.requestVO.FetchingEventListRequestVO;
import com.iamuse.server.requestVO.FileVO;
import com.iamuse.server.requestVO.IOSTranscationsDetailsRequestVO;
import com.iamuse.server.requestVO.ImageVO;
import com.iamuse.server.requestVO.LoginBoothAdminRegistrationRequestVO;
import com.iamuse.server.requestVO.RGBValueRequestVO;
import com.iamuse.server.requestVO.RestartVO;
import com.iamuse.server.requestVO.SubscriptionRequestVO;
import com.iamuse.server.requestVO.UploadImageRequestVO;
import com.iamuse.server.requestVO.UploadImageWithEmailRequestVO;
import com.iamuse.server.responseVO.AppleReceiptVerifyResponse;
import com.iamuse.server.responseVO.BaseResponseVO;
import com.iamuse.server.responseVO.EventFetchingBaseResponseVO;
import com.iamuse.server.responseVO.LoginBaseResponseVO;
import com.iamuse.server.responseVO.SubscriptionMasterResponseVO;
import com.iamuse.server.service.UserService;
import com.iamuse.server.util.DateUtils;
import com.iamuse.server.util.IAmuseUtil;
import com.iamuse.server.util.MailUtil;
import com.iamuse.server.util.PushNotificationTaskRestart;
import java.lang.ArrayIndexOutOfBoundsException;






@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private MessageSource messageSource;
	
	private Locale locale = LocaleContextHolder.getLocale();
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private MailUtil mailUtil;
	
	
	@Autowired
	SessionFactory sessionFactory;
	
	
public int uploadImage(UploadImageRequestVO uploadImageRequestVO,String path,HttpServletRequest request) {
		
		boolean result= false;
		String url =null;
		
		String image = uploadImageRequestVO.getImages();
			String userId=uploadImageRequestVO.getUserId();
			
			Integer imageId=userDao.uploadImage(path);
			
//			String rootPath = System.getProperty("catalina.home");
			//String rootPath = request.getRealPath("../");
			
			String rootPath = null;
			try {
			 rootPath = new java.io.File(request.getSession().getServletContext().getRealPath("")+"/..").getCanonicalPath();
			} catch (java.io.IOException e) {
			 e.printStackTrace();
			}
			if(imageId!=0)
			{
				File file=new File(rootPath+path+"/"+uploadImageRequestVO.getUserId());
			if (!file.exists()) {
				file.mkdir();
			}
			System.out.println("file.getAbsolutePath() \t"+file.getAbsolutePath());
			url= IAmuseUtil.writeFile(image, file.getAbsolutePath(), imageId,"jpg");
			userDao.updateImageName(imageId,path,userId);
			return imageId;
			}
			
		
return imageId;
}

public BoothAdminLogin getDefaultRGBValue(RGBValueRequestVO rbRgbValueRequestVO){
	
	BoothAdminLogin boothAdminLogin=userDao.getDefaultRGBValue(rbRgbValueRequestVO);

return boothAdminLogin;
}
public boolean saveDeviceToken(DeviceTokenRequestVO deviceTokenRequestVO) {
	boolean result= false;
    result= userDao.saveDeviceToken(deviceTokenRequestVO);
	return result;
}
public UploadImage getImageDetails(int imageId){
	UploadImage uploadImage=userDao.getImageDetails(imageId);
	return uploadImage;
}





//public int uploadImageWithEmailId(UploadImageWithEmailRequestVO uploadImageWithEmailRequestVO,String path) {
//	boolean result= false;
//	String url =null;
//	int i=0;
//	String name=null;
//	String rootPath = System.getProperty("catalina.home");
//	String userId=uploadImageWithEmailRequestVO.getUserId();
//	Integer imageId=userDao.uploadImageWithEmailId("/IAmuseimages/EmailImages",uploadImageWithEmailRequestVO);
//    List<String> imagesNames = new ArrayList<String>();
//    if (imageId != 0) {
//    	List<ImageVO> images = uploadImageWithEmailRequestVO.getImages();
//		for (ImageVO image : images)
//		{
//			i++;
//			if (null != image){
//				 url= IAmuseUtil.writeFile(image.getImage(), path+"/"+imageId, i,"jpg");
//				// File file = new File(url+"/"+i+".jpg");
//				   byte[] bytes;
//						//log.info(bytes.length);
//					//	if (bytes.length != 0) {
//							try {
//								bytes = scale(100,100,url+"/"+i+".jpg");
//								IAmuseUtil.writeFile(bytes, rootPath+"/IAmuseimages/EmailImagesThumbnail/"+imageId, ""+i,"jpg");
//							} catch (ApplicationException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//					//}
//				if (i == 1) {
//					 name= i + ".jpg";
//				} else {
//					name=name+ ","+  i + ".jpg";
//			}
//		}
//		}	
//		result=userDao.updateImageNameForEmailId(imageId,name,userId);
//		if(result==true)
//		{
//		try{
//				mailUtil.sendEmail("IAMUSE <apps@iamuse.com>",uploadImageWithEmailRequestVO.getEmailId().trim(),path,"Your Picture Is Ready","IAMUSE"+imageId+".jpg",url,i,true);
//				userDao.updateEmailSendTime(imageId,userId);
//				imageId=5;
//			}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		}
//		}
//		return imageId;
	
	

public int uploadImageWithEmailId(UploadImageWithEmailRequestVO uploadImageWithEmailRequestVO,String path,HttpServletRequest request) {	
	Integer result= 0;
	String url =null;
	int i=0;
	String name=null;
	String imageName = null;
	String emailBody="";
	String rootPath = null;
	try {
	 rootPath = new java.io.File(request.getSession().getServletContext().getRealPath("")+"/..").getCanonicalPath();
	} catch (java.io.IOException e) {
	 e.printStackTrace();
	}
	String userId=uploadImageWithEmailRequestVO.getUserId();
    List<String> imagesNames = new ArrayList<String>();
    	List<ImageVO> images = uploadImageWithEmailRequestVO.getImages();
		Integer eventId=images.get(0).getEventId();
		int l=0;
		for (ImageVO image : images)
		{
			String k= IAmuseUtil.generateImageName();
			i++;
			int j=0;
			if (null != image){
				 url= IAmuseUtil.writeFileImageUpload(image.getImage(), path+"/"+uploadImageWithEmailRequestVO.getImages().get(j).getEventId(), k,"jpg");
				   byte[] bytes;
							try {
								bytes = scale(319,196,url+"/"+k+".jpg");
								IAmuseUtil.writeFile(bytes, rootPath+"/IAmuseimages/EmailImagesThumbnail/"+uploadImageWithEmailRequestVO.getImages().get(j).getEventId(), ""+k,"jpg");
							} catch (ApplicationException e) {
								e.printStackTrace();
							}
							name= k + ".jpg";
							result=userDao.updateImageNameForEmailId(name,userId,image.getEventId(),"/IAmuseimages/EmailImages",uploadImageWithEmailRequestVO,image.getDefaultId(),image.getPicId());
		}
		if(result!=0)
		{
			if(l==0){
					imageName=k+".jpg";
			}else{
				imageName=imageName+","+k+".jpg";
			}
		}
j=j+1;
l++;
		}
try{
		Adminboothevent adminboothevent=userDao.getAdminBoothEvent(Integer.parseInt(uploadImageWithEmailRequestVO.getUserId()),eventId);
		if(("null").equals(adminboothevent.getEmailBody()) || ("").equals(adminboothevent.getEmailBody())){
			emailBody="Thank you for coming to our "+adminboothevent.getEventName()+" ! Here is a picture to keep as a memory of the event. We hope you had fun!";
		}else{
			emailBody=adminboothevent.getEmailBody();
		}
		String testText="<html><body>"+
				"<table id=\"m_-5368927744985068358backgroundTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"+
				"<tbody><tr><td><table id=\"m_-5368927744985068358innerTable\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"+
				"<tbody><tr><td class=\"m_-5368927744985068358payloadCell\" valign=\"top\"><table style=\"border:1px solid rgb(207,207,207);border-radius:8px;background:rgb(255,255,255)\" border=\"0\" cellspacing=\"0\" width=\"100%\">"+
				"<tbody><tr><td style=\"color:rgb(85,85,85);font-size:14px;font-family:'helvetica neue',arial,serif;padding:30px 10px\" align=\"center\">"+
				"<h1 style=\"color:rgb(68,68,68);text-align:center;margin:0px;padding:0px\">Amusement Attached</h1>"+
				"<p style=\"color:rgb(68,68,68);text-align:center;margin:0px;padding:0px\"><span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT100_com_zimbra_url\"><a href=\"http://www.iamuse.com\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://www.iamuse.com&amp;source=gmail&amp;ust=1493380684676000&amp;usg=AFQjCNExSsY9fpbaIXKUYmJaDURNeFlELw\"><img alt=\"iAmuse\" longdesc=\"https://ci3.googleusercontent.com/proxy/tSnkDkFiofgBYd5c5rsqAFQE_sTYbRIdlGOTJCekl9GkbR2Yz4vb0tMUMQ=s0-d-e1-ft#http://www.iamuse.com\" height=\"150\" width=\"250\" src=\"https://ci4.googleusercontent.com/proxy/Tb3FolU3pckmvrJiL0wwXYAjkiQyFtJ4j20cvBuLiCBiboKcgoukvKSeEKteIhruB7KsiifGnmIYyR7Vcwiyr7v07EQaxOJoaHuDjkw=s0-d-e1-ft#http://iamuse-kiosk.appspot.com/images/iamuse-logo2.png\" class=\"CToWUd\"></a></span></p>"+
			    "<p style=\"font-weight:600;font-size:16px\">"+emailBody+"</p><p style=\"font-size:15px;margin-top:35px\"><span style=\"font-weight:bold;margin-right:3px\">Event Host Name :</span> "+adminboothevent.getSponsorName()+"</p>"+
			    "<p style=\"font-size:15px\"><span style=\"font-weight:bold;margin-right:3px\">Event Date :</span> "+adminboothevent.getEventStart()+"</p><p style=\"font-size:15px\"><span style=\"font-weight:bold;margin-right:3px\">Event Host's email address :</span> "+adminboothevent.getEventHostMailerId()+"</p>"+
			    "<p style=\"font-size:15px\"><span style=\"font-weight:bold;margin-right:3px\">Facebook :</span><a href=\""+adminboothevent.getFacebook()+"\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://adminboothevent.getFacebook();&amp;source=gmail&amp;ust=1493380684676000&amp;usg=AFQjCNGumqOxdBAoecShS9vqcuckJavPCA\">"+adminboothevent.getFacebook()+"</a>"+
			    "</p><p style=\"font-size:15px;margin-bottom:35px\"><span style=\"font-weight:bold;margin-right:3px\">Twitter :</span><a href=\""+adminboothevent.getTwitter()+"\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://adminboothevent.getTwitter();&amp;source=gmail&amp;ust=1493380684676000&amp;usg=AFQjCNFDLaMsnpw0j20w_N4xRU_loYQW4Q\">"+adminboothevent.getTwitter()+"</a></p>"+
			    "<h3 style=\"color:rgb(68,68,68);text-align:center;margin:0px;padding:0px\">We are pleased to deliver the picture you took with us.</h3><p>If you like it, spread the word</p><table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"100%\">"+
                "<tbody><tr><td align=\"center\"> <p><span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT101_com_zimbra_url\"><a href=\"https://www.facebook.com/iamusebooth\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/iamusebooth&amp;source=gmail&amp;ust=1493380684677000&amp;usg=AFQjCNFLfToZNO2UisyTB9FWtiPfUFEhcA\"><img alt=\"\" height=\"77\" width=\"78\" src=\"https://ci5.googleusercontent.com/proxy/QgfU2y23FrIXterFADbYO6zHV-jy-Q77H7tdk1antUNK7QB_pJnh70wJsD5eRvf-SNxKFGxkGbyZ48O38acnes2gH8Z3JUoiRZAIhEg-gKU=s0-d-e1-ft#http://iamuse-kiosk.appspot.com/images/social/facebook.png\" class=\"CToWUd\"></a></span></p><p>Like us on Facebook</p></td>"+
                "<td align=\"center\"><p><span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT102_com_zimbra_url\"><a href=\"http://instagram.com/iamusepics\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://instagram.com/iamusepics&amp;source=gmail&amp;ust=1493380684677000&amp;usg=AFQjCNF5OifpP40I6GUbWMZA7Wq8I0Y4mw\"><img alt=\"\" height=\"76\" width=\"77\" src=\"https://ci6.googleusercontent.com/proxy/nWsavTIlx5fyu7OeMazk3LCxbs5zE_ooib6Ge2H6zYXQd4Q7Nc5f661dn9_rGpnBXD7UX8VSTNciK2FMYtqmrlfqoz4qMycEGRFxo4vqZWbW=s0-d-e1-ft#http://iamuse-kiosk.appspot.com/images/social/instagram.png\" class=\"CToWUd\"></a></span></p><p>Follow us on Instagram</p></td>"+
                "<td align=\"center\"><p><span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT103_com_zimbra_url\"><a href=\"http://instagram.com/iamusepics\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://instagram.com/iamusepics&amp;source=gmail&amp;ust=1493380684677000&amp;usg=AFQjCNF5OifpP40I6GUbWMZA7Wq8I0Y4mw\"><img alt=\"\" height=\"77\" width=\"78\" src=\"https://ci3.googleusercontent.com/proxy/C8UwfqvdWNrRWij9MwBeGbrD1MgAvx5E1fO-a1JpCaIHYFn7oWDXD7YaGQKCdFu-kOF4bqMhC6dRi7lAoznpM2fLiUdfUxCXlzSpwNyC9A=s0-d-e1-ft#http://iamuse-kiosk.appspot.com/images/social/twitter.png\" class=\"CToWUd\"></a></span></p><p>Follow us on Twitter</p></td>"+
                "</tr></tbody></table>"+
                "<p>Visit our website <span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT104_com_zimbra_url\"><a href=\"http://www.iamuse.com\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://www.iamuse.com&amp;source=gmail&amp;ust=1493380684677000&amp;usg=AFQjCNE_v-p9Y1LQV-DpIv5GqwYEJDT-rQ\">www.iamuse.com</a></span></p>"+
                "</td></tr></tbody></table></td></tr><tr>"+
                "<td class=\"m_-5368927744985068358payloadCell\" style=\"height:40px;font-size:9px;font-family:'helvetica neue',arial,serif;color:rgb(136,136,136)\" align=\"right\" valign=\"top\"><span class=\"m_-5368927744985068358Object\" role=\"link\" id=\"m_-5368927744985068358OBJ_PREFIX_DWT105_com_zimbra_url\"><a style=\"color:rgb(136,136,136)\" href=\"http://iamuse.com\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=en&amp;q=http://iamuse.com&amp;source=gmail&amp;ust=1493380684677000&amp;usg=AFQjCNHuUfOsnIEfdwOnnQQ9sl7Ljgn9ZA\">powered by iAmuse.com</a></span></td>"+
                "</tr></tbody></table></td></tr></tbody></table></body></html>";
		
		 /*String testText="\n\n\t"+emailBody+
					        "\n\n\t Event Host Name : " +adminboothevent.getEventName()+
					        "\n\n\t Event Date : " +  adminboothevent.getEventStart()+
					        "\n\n\t Event Host's email address : "+adminboothevent.getEventHostMailerId()+
					        "\n\t Others" +"\n"+
					        "\n\n\t Facebook : " +adminboothevent.getFacebook() +
					        "\n\n\t Twitter : " +adminboothevent.getTwitter()  ;*/
		   	String mailerHostAddress=adminboothevent.getEventHostMailerId();
		    mailUtil.sendEmailUploadMail(adminboothevent.getSponsorName()+"("+mailerHostAddress+")"+" <apps@iamuse.com>",uploadImageWithEmailRequestVO.getEmailId().trim(),path,"Your Picture Is Ready","IAMUSE.jpg",url,imageName,true,testText);
		    userDao.updateEmailSendTime(imageName,userId);
		    userDao.updateStatusCount(uploadImageWithEmailRequestVO.getEmailId().trim(),eventId);
			result=5;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
		}

public int crashlogsupload(CrashLogsRequestVO crashLogsRequestVO, String path,HttpServletRequest request) {
	String url=null;
	String name=null;
	Integer id = userDao.crashlogsupload(path);
	String rootPath = System.getProperty("catalina.home");
	//String rootPath = request.getRealPath("../");
	if (id != 0) {
		List<FileVO> files = crashLogsRequestVO.getFiles();
		int i = 0;
		for (FileVO file : files)
		{
			i++;
			if (null != file){
				
				 url= IAmuseUtil.writeFile(file.getFile(), rootPath+path+"/"+id, i,"crash");
				if (i == 1) {
					 name= i + ".crash";
				} else {
					name=name+ ","+  i + ".crash";
			}
				
		}
		}	
		String userId = crashLogsRequestVO.getUserId();
		userDao.crashlogsuploadName(id, name, userId);
		return id;
		
	}
	return id;
}

public boolean saveDeviceIP(DeviceIPRequestVO deviceIPRequestVO) {
	boolean result= false;
    result= userDao.saveDeviceIP(deviceIPRequestVO);
	return result;
}

public DeviceIp getDeviceIP(DeviceIPRequestVO deviceIPRequestVO) {
	DeviceIp deviceIp=userDao.getDeviceIP(deviceIPRequestVO);
	return deviceIp;
}



public byte[] scale(int width, int height, String imageUrl) throws ApplicationException , ArrayIndexOutOfBoundsException {
	   //  ByteArrayInputStream in = new ByteArrayInputStream(bytes);
	     try {
	    	 File file = new File(imageUrl);
	    	 FileInputStream fis = new FileInputStream(file); 
	      BufferedImage img = ImageIO.read(fis);
	     System.out.println("fwfew");
	      BufferedImage imageBuff = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC, width,height, Scalr.OP_ANTIALIAS);
	      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	   //   log.info("scallling........."+imageBuff);
	      ImageIO.write(imageBuff, "png", buffer);

	      return buffer.toByteArray();
	     } catch (IOException e) {
	      throw new ApplicationException("IOException in scale", null);
	     }
	    }
@Override
public LoginBaseResponseVO saveAdminBoothRegistration(BoothAdminRegistrationRequestVO adminBoothRegistrationRequestVO) {
	return userDao.saveAdminBoothRegistration(adminBoothRegistrationRequestVO);
}

@Override
public LoginBaseResponseVO fetchLoginBaseResponseVO(
		LoginBoothAdminRegistrationRequestVO loginRegistrationRequestVO) {
	return userDao.fetchLoginBaseResponseVO(loginRegistrationRequestVO);
}

@Override
public EventFetchingBaseResponseVO fetchEventFetchingAdminBooth(FetchingEventListRequestVO fetchinfEventAdminBoothRequestVo) {
	return userDao.fetchEventFetchingAdminBooth(fetchinfEventAdminBoothRequestVo);
}

@Override
public String deviceRegisterSevice(DeviceRegistrationRequestVO deviceRegistrationRequestVO) {
	return userDao.deviceRegisterSevice(deviceRegistrationRequestVO);
}

@Override
public SubscriptionMasterResponseVO fetchSubscriptionsMasterList(SubscriptionRequestVO subscription) {
	SubscriptionMaster masterList=userDao.fetchSubscriptionsMasterList(subscription);
	SubscriptionMasterResponseVO responseVO=new SubscriptionMasterResponseVO();
	  ObjectMapper oMapper = new ObjectMapper();
	if (masterList!=null) {
		BoothAdminLogin userDetails=userDao.getBoothAdminLoginById(subscription.getUserId());
		HttpClient client = new DefaultHttpClient();
		 String productImaUse="https://subscriptions.zoho.com/api/v1/products";
         HttpGet getProductRequest=createHttpGet(productImaUse);
         try {
         HttpResponse getIamUseProductResponse =client.execute(getProductRequest);
         BufferedReader rd = new BufferedReader(new InputStreamReader(getIamUseProductResponse.getEntity().getContent()));
         String line = "";
         while ((line = rd.readLine()) != null) {
        	  JSONParser j = new JSONParser();
              JSONObject o = (JSONObject)j.parse(line);
              if(getIamUseProductResponse.getStatusLine().getStatusCode()==200) {
            	  List<Object> productList = (List<Object>)o.get("product");
            	  if(productList!=null && !productList.isEmpty()) {
            		  productList.stream().filter(p->Objects.nonNull(p)).forEach(q->{
            			  Map<String, Object> map = oMapper.convertValue(q, Map.class);
            			  System.out.println(map.get("product_id"));
            		  });
            	  }else {
            		  String createProductURL="https://subscriptions.zoho.com/api/v1/products";
            		  JSONObject createProductjson = new JSONObject();
            		  createProductjson.put("name", "iamuse");
            			
            		  HttpPost createProductPostRequest=createHttpPostAlongJson(createProductURL, createProductjson);
            		  getIamUseProductResponse =client.execute(createProductPostRequest);
            		   rd = new BufferedReader(new InputStreamReader(getIamUseProductResponse.getEntity().getContent()));
            	          line = "";
            	         while ((line = rd.readLine()) != null) {
            	        	 
            	              JSONObject productJson = (JSONObject)j.parse(line);
            	              if(getIamUseProductResponse.getStatusLine().getStatusCode()==201) {
            	            	  Map<String, Object> imamUserProductJson = (Map)o.get("product");
            	              }
            	         }
            	              
            	  }
            	  
              }
         }
         System.out.println(getIamUseProductResponse.getStatusLine().getStatusCode());
         
         }catch (Exception e) {
			// TODO: handle exception
		}
		
		
		String url = "https://subscriptions.zoho.com/api/v1/customers";
	
		
		JSONObject json = new JSONObject();
		json.put("display_name", userDetails.getUsername());
		json.put("email", userDetails.getEmailId());
		
		
		HttpPost request =createHttpPostAlongJson(url, json);
		try {
			request.setEntity(new StringEntity(json.toJSONString()));
			HttpResponse r =client.execute(request);
			System.out.println(r.getStatusLine().getStatusCode());
	            BufferedReader rd = new BufferedReader(new InputStreamReader(r.getEntity().getContent()));
	            String line = "";
	            while ((line = rd.readLine()) != null) {
	               //Parse our JSON response               
	               JSONParser j = new JSONParser();
	               JSONObject o = (JSONObject)j.parse(line);
	               if(r.getStatusLine().getStatusCode()==201) {
	            	   Map<String, Object> response = (Map)o.get("customer");
	            	   
	            	  
		               String plannUrl="https://subscriptions.zoho.com/api/v1/plans";
		               JSONObject json2 = new JSONObject();
			    		json2.put("plan_code", response.get("customer_id"));
			    		json2.put("name", response.get("customer_id"));
			    		json2.put("recurring_price", response.get("customer_id"));
		               HttpPost request2 = createHttpPostAlongJson(plannUrl, json2);
		               
		               
		             
		               
		               /*String subscriptionUrl="https://subscriptions.zoho.com/api/v1/subscriptions";
		               HttpPost request2 = new HttpPost(subscriptionUrl);
		               request2.setHeader("Content-Type", "application/json");
		               request2.setHeader("Authorization", "Zoho-authtoken 3241cfd56afbc162bed1af6b7f47902c");
		               request2.setHeader("X-com-zoho-subscriptions-organizationid", "690292259");
		       		
		       		JSONObject json2 = new JSONObject();
		    		json2.put("customer_id", response.get("customer_id"));
		    		
		    		try {
		    			request2.setEntity(new StringEntity(json2.toJSONString()));
		    			HttpResponse rs =client.execute(request2);
		    			System.out.println(rs.getStatusLine().getStatusCode());
		    		}catch (Exception ex) {
		    			ex.printStackTrace();
					}*/
		    		
	               }else {
	            	   Long codeResponse = (Long) o.get("code");
	            	   if(codeResponse==3062) {
	            		   System.out.println("Duploicate");
	            	   }
	            	   
	               }
	               
	              
	 
	            }
				 
		} catch (IOException | ParseException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(ArrayIndexOutOfBoundsException ww)
		{
			ww.printStackTrace();
		}
		
		
		
	responseVO.setCreatedDate(DateUtils.timeStampConvertIntoStringDateFormat(masterList.getCreatedDate()));
	
	if(masterList.getCreatedUserId()!=null){
	responseVO.setCreatedUserId(masterList.getCreatedUserId());
	}
	
	responseVO.setStatus(masterList.getStatus());
	responseVO.setSubId(masterList.getSubId());
	responseVO.setSubName(masterList.getSubName());
	responseVO.setSubPrice(masterList.getSubPrice());
	responseVO.setSubValidaityDayPeriod(masterList.getSubValidaityDayPeriod());
	if(masterList.getUpdatedByUserId()!=null){
	responseVO.setUpdatedByUserId(masterList.getUpdatedByUserId());
	}
	if(masterList.getUpdatedDate()!=null){
	responseVO.setUpdatedDate(DateUtils.timeStampConvertIntoStringDateFormat(masterList.getUpdatedDate()));
	}
	
	if(masterList.getStatus()!=null){
		responseVO.setStatus(masterList.getStatus());
	}
	
	if(masterList.getIsDeleted()!=null){
		responseVO.setIsDeleted(masterList.getIsDeleted());
	}
	responseVO.setResponseCode("1");
	responseVO.setResponseDescription("Success");
	}else{
		responseVO.setResponseCode("0");
		responseVO.setResponseDescription("Fail");
	}
	
	return responseVO;
}

@Override
public BaseResponseVO saveTranscationIOSDetails(IOSTranscationsDetailsRequestVO iosTrxDetailsBasedUserId,AppleReceiptVerifyResponse responseVO) {
	
	
	TransactionMaster  trxMaster=new TransactionMaster();
	trxMaster.setStatus(true);
	trxMaster.setIsDeleted(false);
	trxMaster.setTxnType("IOS");
	
	trxMaster.setProductId(responseVO.getReceipt().getProduct_id());
	trxMaster.setOriginalpurchasedatems(DateUtils.stringDateFormatYYYYMMDDHHMMSS(responseVO.getReceipt().getOriginal_purchase_date_ms().toString()));
	trxMaster.setPurchasedatepst(responseVO.getReceipt().getPurchase_date_pst());
	trxMaster.setOriginalpurchasedate(responseVO.getReceipt().getOriginal_purchase_date());
	trxMaster.setBvrs(responseVO.getReceipt().getBvrs());
	trxMaster.setTxnId(responseVO.getReceipt().getTransaction_id());
	trxMaster.setOriginalpurchasedatepst(responseVO.getReceipt().getOriginal_purchase_date_pst());
	trxMaster.setUniqueidentifier(responseVO.getReceipt().getUnique_identifier());
	trxMaster.setOriginaltransactionid(responseVO.getReceipt().getOriginal_transaction_id());
	trxMaster.setItemid(responseVO.getReceipt().getItem_id());
	trxMaster.setPurchasedatems(DateUtils.stringDateFormatYYYYMMDDHHMMSS(responseVO.getReceipt().getPurchase_date_ms()));
	trxMaster.setQuantity(responseVO.getReceipt().getQuantity());
	trxMaster.setPurchasedate(responseVO.getReceipt().getPurchase_date());
	trxMaster.setBid(responseVO.getReceipt().getBid());
	trxMaster.setUniquevendoridentifier(responseVO.getReceipt().getUnique_vendor_identifier());
	trxMaster.setUserId(iosTrxDetailsBasedUserId.getUserId());
	trxMaster.setPaymentAmount(iosTrxDetailsBasedUserId.getAmount());
	trxMaster.setPaymentGross(iosTrxDetailsBasedUserId.getAmount());
	return userDao.saveTranscationIOSDetails(iosTrxDetailsBasedUserId,trxMaster);
}

public RestartVO restertServer(BaseRequestVO restartVO, PushNotificationTaskRestart taskRestartUpdate) {
	return userDao.restertServer(restartVO,taskRestartUpdate);
}

public Fovbyuser getFobByUser(BaseRequestVO baseRequestVO) {
	return userDao.getFobByUser(baseRequestVO);
}

public List<DeviceRegistration> getRegisteredDevice(Integer userId) {
	return userDao.getRegisteredDevice(userId);
}

@Override
public String logOutService(DeviceRegistrationRequestVO deviceRegistrationRequestVO) {
	return userDao.logOutService(deviceRegistrationRequestVO);
}


private HttpPost createHttpPostAlongJson(String url, JSONObject jsonObject) {
	
	HttpPost request = new HttpPost(url);
	request.setHeader("Content-Type", "application/json");
	request.setHeader("Authorization", "Zoho-authtoken 3241cfd56afbc162bed1af6b7f47902c");
	request.setHeader("X-com-zoho-subscriptions-organizationid", "690292259");
	try {
		request.setEntity(new StringEntity(jsonObject.toJSONString()));	
	}catch (Exception e) {
		request=null;
	}
	
	return request;
}
	
private HttpGet createHttpGet(String url) {
	
	HttpGet request = new HttpGet(url);
	request.setHeader("Content-Type", "application/json");
	request.setHeader("Authorization", "Zoho-authtoken 3241cfd56afbc162bed1af6b7f47902c");
	request.setHeader("X-com-zoho-subscriptions-organizationid", "690292259");
	return request;
}
}